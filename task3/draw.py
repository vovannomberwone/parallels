import sys
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

N = 16
Lx = Ly = Lz = 1.0
T = 1.0


def u(x, y, z, t):
    a_t = math.pi * math.sqrt(9 / (Lx * Lx) + 4 / (Ly * Ly) + 4 / (Lz * Lz))
    return math.sin(3 * math.pi / Lx * x) * math.sin(2 * math.pi / Ly * y) * math.sin(2 * math.pi / Lz * z) * math.cos(a_t * t + 4 * math.pi)


def main():
    if len(sys.argv) != 2:
        print("Usage: python draw.py [--true|--computed|--diff]")
        return

    X = np.linspace(0., Lx, N)
    Y = np.linspace(0., Ly, N)
    Z = np.linspace(0., Lz, N)
    X, Y, Z = np.meshgrid(X, Y, Z, indexing="ij")
    X = X.reshape(-1)
    Y = Y.reshape(-1)
    Z = Z.reshape(-1)
    U = np.array([u(x, y, z, T) for x, y, z in zip(X, Y, Z)])
    with open("log.txt") as f:
        u_computed = np.array([float(l) for l in f if l])

    if sys.argv[1] == "--true":
        vals = U
    elif sys.argv[1] == "--computed":
        vals = u_computed
    elif sys.argv[1] == "--diff":
        vals = abs(U - u_computed)
    else:
        print("Usage: python draw.py [--true|--computed|--diff]")
        return

    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.view_init(45, 60)
    color_map = cm.ScalarMappable(cmap="viridis")
    color_map.set_array(vals)
    ax.scatter(X, Y, Z, c=vals, cmap=color_map.get_cmap())
    plt.colorbar(color_map)
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Z")
    plt.show()


if __name__ == "__main__":
    main()
