#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <mpi.h>
#include <omp.h>
#include <unistd.h>

using namespace std;

int nx_low, nx_high, ny_low, ny_high, nz_low, nz_high, split_type, bmax, bnumx,
    bnumy, bnumz, X, Y, Z, NUM_THREADS = 1024, K = 250;
double dx, dy, dz, Lx, Ly, Lz, x_low, y_low, z_low, T = 1, dt = T / (K - 1);

__host__ __device__ double u(double x, double y, double z, double t, double Lx, double Ly,
                             double Lz) {
    double a_t = M_PI * sqrt(9 / (Lx * Lx) + 4 / (Ly * Ly) + 4 / (Lz * Lz));
    return sin(3 * M_PI / Lx * x) * sin(2 * M_PI / Ly * y) * sin(2 * M_PI / Lz * z) *
           cos(a_t * t + 4 * M_PI);
}

__device__ double phi(double x, double y, double z, double Lx, double Ly, double Lz) {
    return u(x, y, z, 0, Lx, Ly, Lz);
}

__host__ void setup_range(int rank, int N, int num_processes) {
    int cubert = round(cbrt((double)num_processes));
    if (cubert * cubert * cubert == num_processes) {
        // cube
        bnumx = rank / (cubert * cubert);
        bnumy = rank % (cubert * cubert) / cubert;
        bnumz = rank % cubert;
        nx_low = N / cubert * bnumx;
        nx_high = bnumx == cubert - 1 ? N : N / cubert * (bnumx + 1);
        ny_low = N / cubert * bnumy;
        ny_high = bnumy == cubert - 1 ? N : N / cubert * (bnumy + 1);
        nz_low = N / cubert * bnumz;
        nz_high = bnumz == cubert - 1 ? N : N / cubert * (bnumz + 1);
        split_type = 0;
        bmax = cubert;
    } else {
        // line
        bnumx = rank;
        bnumy = bnumz = 0;
        nx_low = N / num_processes * bnumx;
        nx_high = bnumx == num_processes - 1 ? N : N / num_processes * (bnumx + 1);
        ny_low = 0;
        ny_high = N;
        nz_low = 0;
        nz_high = N;
        split_type = 1;
        bmax = num_processes;
    }
    dx = Lx / (N - 1);
    dy = Ly / (N - 1);
    dz = Lz / (N - 1);
    x_low = nx_low * dx;
    y_low = ny_low * dy;
    z_low = nz_low * dz;
    X = nx_high - nx_low;
    Y = ny_high - ny_low;
    Z = nz_high - nz_low;
}

__host__ int get_rank(int bnumx, int bnumy, int bnumz) {
    if (split_type == 0) {
        return bnumz + bnumy * bmax + bnumx * bmax * bmax;
    } else {
        return bnumx;
    }
}

__global__ void mv_x_left(double *u_n, double *u_n_x_left_d, int X, int Y, int Z) {
    int ind = blockIdx.x * blockDim.x + threadIdx.x;
    if (ind >= Y * Z)
        return;
    int ny = ind / Z;
    int nz = ind % Z;
    u_n_x_left_d[ny * Z + nz] = u_n[0 * Y * Z + ny * Z + nz];
}

__global__ void mv_x_right(double *u_n, double *u_n_x_right_d, int X, int Y, int Z) {
    int ind = blockIdx.x * blockDim.x + threadIdx.x;
    if (ind >= Y * Z)
        return;
    int ny = ind / Z;
    int nz = ind % Z;
    u_n_x_right_d[ny * Z + nz] = u_n[(X - 1) * Y * Z + ny * Z + nz];
}

__global__ void mv_y_left(double *u_n, double *u_n_y_left_d, int X, int Y, int Z) {
    int ind = blockIdx.x * blockDim.x + threadIdx.x;
    if (ind >= X * Z)
        return;
    int nx = ind / Z;
    int nz = ind % Z;
    u_n_y_left_d[nx * Z + nz] = u_n[nx * Y * Z + 0 * Z + nz];
}

__global__ void mv_y_right(double *u_n, double *u_n_y_right_d, int X, int Y, int Z) {
    int ind = blockIdx.x * blockDim.x + threadIdx.x;
    if (ind >= X * Z)
        return;
    int nx = ind / Z;
    int nz = ind % Z;
    u_n_y_right_d[nx * Z + nz] = u_n[nx * Y * Z + (Y - 1) * Z + nz];
}

__global__ void mv_z_left(double *u_n, double *u_n_z_left_d, int X, int Y, int Z) {
    int ind = blockIdx.x * blockDim.x + threadIdx.x;
    if (ind >= X * Y)
        return;
    int nx = ind / Y;
    int ny = ind % Y;
    u_n_z_left_d[nx * Y + ny] = u_n[nx * Y * Z + ny * Z + 0];
}

__global__ void mv_z_right(double *u_n, double *u_n_z_right_d, int X, int Y, int Z) {
    int ind = blockIdx.x * blockDim.x + threadIdx.x;
    if (ind >= X * Y)
        return;
    int nx = ind / Y;
    int ny = ind % Y;
    u_n_z_right_d[nx * Y + ny] = u_n[nx * Y * Z + ny * Z + (Z - 1)];
}

__host__ int get_num_blocks(int num_elems, int num_threads) {
    return (num_elems - 1) / num_threads + 1;
}

__host__ void sendborder(double *u_n, double *u_n_x_left_send_h, double *u_n_y_left_send_h,
                         double *u_n_z_left_send_h, double *u_n_x_left_recv_h,
                         double *u_n_y_left_recv_h, double *u_n_z_left_recv_h,
                         double *u_n_x_right_send_h, double *u_n_y_right_send_h,
                         double *u_n_z_right_send_h, double *u_n_x_right_recv_h,
                         double *u_n_y_right_recv_h, double *u_n_z_right_recv_h,
                         double *u_n_x_left_d, double *u_n_y_left_d, double *u_n_z_left_d,
                         double *u_n_x_right_d, double *u_n_y_right_d, double *u_n_z_right_d) {
    MPI_Request requests[12];
    MPI_Status statuses[12];
    int num_requests = 0, num_blocks;

    // d2h
    if (split_type == 0) {
        if (bnumx != 0) {
            num_blocks = get_num_blocks(Y * Z, NUM_THREADS);
            mv_x_left<<<num_blocks, NUM_THREADS>>>(u_n, u_n_x_left_d, X, Y, Z);
            cudaMemcpyAsync(u_n_x_left_send_h, u_n_x_left_d, Y * Z * sizeof(double),
                            cudaMemcpyDeviceToHost);
        }
        if (bnumx != bmax - 1) {
            num_blocks = get_num_blocks(Y * Z, NUM_THREADS);
            mv_x_right<<<num_blocks, NUM_THREADS>>>(u_n, u_n_x_right_d, X, Y, Z);
            cudaMemcpyAsync(u_n_x_right_send_h, u_n_x_right_d, Y * Z * sizeof(double),
                            cudaMemcpyDeviceToHost);
        }
        if (bnumy != 0) {
            num_blocks = get_num_blocks(X * Z, NUM_THREADS);
            mv_y_left<<<num_blocks, NUM_THREADS>>>(u_n, u_n_y_left_d, X, Y, Z);
            cudaMemcpyAsync(u_n_y_left_send_h, u_n_y_left_d, X * Z * sizeof(double),
                            cudaMemcpyDeviceToHost);
        }
        if (bnumy != bmax - 1) {
            num_blocks = get_num_blocks(X * Z, NUM_THREADS);
            mv_y_right<<<num_blocks, NUM_THREADS>>>(u_n, u_n_y_right_d, X, Y, Z);
            cudaMemcpyAsync(u_n_y_right_send_h, u_n_y_right_d, X * Z * sizeof(double),
                            cudaMemcpyDeviceToHost);
        }
        if (bnumz != 0) {
            num_blocks = get_num_blocks(X * Y, NUM_THREADS);
            mv_z_left<<<num_blocks, NUM_THREADS>>>(u_n, u_n_z_left_d, X, Y, Z);
            cudaMemcpyAsync(u_n_z_left_send_h, u_n_z_left_d, X * Y * sizeof(double),
                            cudaMemcpyDeviceToHost);
        }
        if (bnumz != bmax - 1) {
            num_blocks = get_num_blocks(X * Y, NUM_THREADS);
            mv_z_right<<<num_blocks, NUM_THREADS>>>(u_n, u_n_z_right_d, X, Y, Z);
            cudaMemcpyAsync(u_n_z_right_send_h, u_n_z_right_d, X * Y * sizeof(double),
                            cudaMemcpyDeviceToHost);
        }
    } else {
        if (bnumx != 0) {
            num_blocks = get_num_blocks(Y * Z, NUM_THREADS);
            mv_x_left<<<num_blocks, NUM_THREADS>>>(u_n, u_n_x_left_d, X, Y, Z);
            cudaMemcpyAsync(u_n_x_left_send_h, u_n_x_left_d, Y * Z * sizeof(double),
                            cudaMemcpyDeviceToHost);
        }
        if (bnumx != bmax - 1) {
            num_blocks = get_num_blocks(Y * Z, NUM_THREADS);
            mv_x_right<<<num_blocks, NUM_THREADS>>>(u_n, u_n_x_right_d, X, Y, Z);
            cudaMemcpyAsync(u_n_x_right_send_h, u_n_x_right_d, Y * Z * sizeof(double),
                            cudaMemcpyDeviceToHost);
        }
    }
    cudaDeviceSynchronize();

    // h2h
    if (split_type == 0) {
        if (bnumx != 0) {
            MPI_Isend(u_n_x_left_send_h, Y * Z, MPI_DOUBLE, get_rank(bnumx - 1, bnumy, bnumz), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
            // Irecv for Isend from next if block
            MPI_Irecv(u_n_x_left_recv_h, Y * Z, MPI_DOUBLE, get_rank(bnumx - 1, bnumy, bnumz), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
        }
        if (bnumx != bmax - 1) {
            MPI_Isend(u_n_x_right_send_h, Y * Z, MPI_DOUBLE, get_rank(bnumx + 1, bnumy, bnumz), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
            // Irecv for Isend from next if block
            MPI_Irecv(u_n_x_right_recv_h, Y * Z, MPI_DOUBLE, get_rank(bnumx + 1, bnumy, bnumz), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
        }
        if (bnumy != 0) {
            MPI_Isend(u_n_y_left_send_h, X * Z, MPI_DOUBLE, get_rank(bnumx, bnumy - 1, bnumz), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
            // Irecv for Isend from next if block
            MPI_Irecv(u_n_y_left_recv_h, X * Z, MPI_DOUBLE, get_rank(bnumx, bnumy - 1, bnumz), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
        }
        if (bnumy != bmax - 1) {
            MPI_Isend(u_n_y_right_send_h, X * Z, MPI_DOUBLE, get_rank(bnumx, bnumy + 1, bnumz), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
            // Irecv for Isend from next if block
            MPI_Irecv(u_n_y_right_recv_h, X * Z, MPI_DOUBLE, get_rank(bnumx, bnumy + 1, bnumz), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
        }
        if (bnumz != 0) {
            MPI_Isend(u_n_z_left_send_h, X * Y, MPI_DOUBLE, get_rank(bnumx, bnumy, bnumz - 1), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
            // Irecv for Isend from next if block
            MPI_Irecv(u_n_z_left_recv_h, X * Y, MPI_DOUBLE, get_rank(bnumx, bnumy, bnumz - 1), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
        }
        if (bnumz != bmax - 1) {
            MPI_Isend(u_n_z_right_send_h, X * Y, MPI_DOUBLE, get_rank(bnumx, bnumy, bnumz + 1), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
            // Irecv for Isend from previous if block
            MPI_Irecv(u_n_z_right_recv_h, X * Y, MPI_DOUBLE, get_rank(bnumx, bnumy, bnumz + 1), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
        }
    } else {
        if (bnumx != 0) {
            MPI_Isend(u_n_x_left_send_h, Y * Z, MPI_DOUBLE, get_rank(bnumx - 1, bnumy, bnumz), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
            // Irecv for Isend from next if block
            MPI_Irecv(u_n_x_left_recv_h, Y * Z, MPI_DOUBLE, get_rank(bnumx - 1, bnumy, bnumz), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
        }
        if (bnumx != bmax - 1) {
            MPI_Isend(u_n_x_right_send_h, Y * Z, MPI_DOUBLE, get_rank(bnumx + 1, bnumy, bnumz), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
            // Irecv for Isend from previous if block
            MPI_Irecv(u_n_x_right_recv_h, Y * Z, MPI_DOUBLE, get_rank(bnumx + 1, bnumy, bnumz), 0,
                      MPI_COMM_WORLD, &requests[num_requests++]);
        }
    }
    MPI_Waitall(num_requests, requests, statuses);

    // h2d
    if (split_type == 0) {
        if (bnumx != 0) {
            cudaMemcpyAsync(u_n_x_left_d, u_n_x_left_recv_h, Y * Z * sizeof(double),
                            cudaMemcpyHostToDevice);
        }
        if (bnumx != bmax - 1) {
            cudaMemcpyAsync(u_n_x_right_d, u_n_x_right_recv_h, Y * Z * sizeof(double),
                            cudaMemcpyHostToDevice);
        }
        if (bnumy != 0) {
            cudaMemcpyAsync(u_n_y_left_d, u_n_y_left_recv_h, X * Z * sizeof(double),
                            cudaMemcpyHostToDevice);
        }
        if (bnumy != bmax - 1) {
            cudaMemcpyAsync(u_n_y_right_d, u_n_y_right_recv_h, X * Z * sizeof(double),
                            cudaMemcpyHostToDevice);
        }
        if (bnumz != 0) {
            cudaMemcpyAsync(u_n_z_left_d, u_n_z_left_recv_h, X * Y * sizeof(double),
                            cudaMemcpyHostToDevice);
        }
        if (bnumz != bmax - 1) {
            cudaMemcpyAsync(u_n_z_right_d, u_n_z_right_recv_h, X * Y * sizeof(double),
                            cudaMemcpyHostToDevice);
        }
    } else {
        if (bnumx != 0) {
            cudaMemcpyAsync(u_n_x_left_d, u_n_x_left_recv_h, Y * Z * sizeof(double),
                            cudaMemcpyHostToDevice);
        }
        if (bnumx != bmax - 1) {
            cudaMemcpyAsync(u_n_x_right_d, u_n_x_right_recv_h, Y * Z * sizeof(double),
                            cudaMemcpyHostToDevice);
        }
    }
}

__device__ double nabla(double u_n_x_y_z, double u_n_x_left, double u_n_x_right, double u_n_y_left,
                        double u_n_y_right, double u_n_z_left, double u_n_z_right, double dx,
                        double dy, double dz) {
    return (u_n_x_left - 2 * u_n_x_y_z + u_n_x_right) / (dx * dx) +
           (u_n_y_left - 2 * u_n_x_y_z + u_n_y_right) / (dy * dy) +
           (u_n_z_left - 2 * u_n_x_y_z + u_n_z_right) / (dz * dz);
}

__host__ __device__ int on_global_border(int nx, int ny, int nz, int bnumx, int bnumy, int bnumz, int bmax,
                               int split_type, int X, int Y, int Z) {
    if (split_type == 0) {
        return (nx == 0 and bnumx == 0) or (nx == X - 1 and bnumx == bmax - 1) or
               (ny == 0 and bnumy == 0) or (ny == Y - 1 and bnumy == bmax - 1) or
               (nz == 0 and bnumz == 0) or (nz == Z - 1 and bnumz == bmax - 1);
    } else {
        return (nx == 0 and bnumx == 0) or (nx == X - 1 and bnumx == bmax - 1) or (ny == 0) or
               (ny == Y - 1) or (nz == 0) or (nz == Z - 1);
    }
}

__device__ double border(double x, double y, double z, double t, double Lx, double Ly, double Lz) {
    return u(x, y, z, t, Lx, Ly, Lz);
}

__global__ void zero_step_kernel(double *u_n, double x_low, double y_low, double z_low, double dx,
                                 double dy, double dz, double Lx, double Ly, double Lz, int X,
                                 int Y, int Z) {
    int ind = blockIdx.x * blockDim.x + threadIdx.x;
    if (ind >= X * Y * Z)
        return;
    int nx = ind / (Y * Z);
    int ny = ind % (Y * Z) / Z;
    int nz = ind % Z;
    double x = x_low + nx * dx;
    double y = y_low + ny * dy;
    double z = z_low + nz * dz;
    u_n[nx * Y * Z + ny * Z + nz] = phi(x, y, z, Lx, Ly, Lz);
}

__global__ void first_step_kernel(double *u_n, double *u_n_1, double x_low, double y_low,
                                  double z_low, double dx, double dy, double dz, double dt,
                                  double Lx, double Ly, double Lz, int X, int Y, int Z, int bnumx,
                                  int bnumy, int bnumz, int bmax, int split_type,
                                  double *u_n_x_left_d, double *u_n_y_left_d, double *u_n_z_left_d,
                                  double *u_n_x_right_d, double *u_n_y_right_d,
                                  double *u_n_z_right_d) {
    int ind = blockIdx.x * blockDim.x + threadIdx.x;
    if (ind >= X * Y * Z)
        return;
    int nx = ind / (Y * Z);
    int ny = ind % (Y * Z) / Z;
    int nz = ind % Z;
    double x = x_low + nx * dx;
    double y = y_low + ny * dy;
    double z = z_low + nz * dz;
    double u_n_x_left, u_n_y_left, u_n_z_left, u_n_x_right, u_n_y_right, u_n_z_right;
    double u_n_center;

    if (on_global_border(nx, ny, nz, bnumx, bnumy, bnumz, bmax, split_type, X, Y, Z)) {
        u_n_1[nx * Y * Z + ny * Z + nz] = border(x, y, z, dt, Lx, Ly, Lz);
    } else {
        if (split_type == 0) {
            u_n_x_left = nx == 0 ? u_n_x_left_d[ny * Z + nz] : u_n[(nx - 1) * Y * Z + ny * Z + nz];
            u_n_x_right =
                nx == X - 1 ? u_n_x_right_d[ny * Z + nz] : u_n[(nx + 1) * Y * Z + ny * Z + nz];
            u_n_y_left = ny == 0 ? u_n_y_left_d[nx * Z + nz] : u_n[nx * Y * Z + (ny - 1) * Z + nz];
            u_n_y_right =
                ny == Y - 1 ? u_n_y_right_d[nx * Z + nz] : u_n[nx * Y * Z + (ny + 1) * Z + nz];
            u_n_z_left = nz == 0 ? u_n_z_left_d[nx * Y + ny] : u_n[nx * Y * Z + ny * Z + (nz - 1)];
            u_n_z_right =
                nz == Z - 1 ? u_n_z_right_d[nx * Y + ny] : u_n[nx * Y * Z + ny * Z + (nz + 1)];
        } else {
            u_n_x_left = nx == 0 ? u_n_x_left_d[ny * Z + nz] : u_n[(nx - 1) * Y * Z + ny * Z + nz];
            u_n_x_right =
                nx == X - 1 ? u_n_x_right_d[ny * Z + nz] : u_n[(nx + 1) * Y * Z + ny * Z + nz];
            u_n_x_right =
                nx == X - 1 ? u_n_x_right_d[ny * Z + nz] : u_n[(nx + 1) * Y * Z + ny * Z + nz];
            u_n_y_left = u_n[nx * Y * Z + (ny - 1) * Z + nz];
            u_n_y_right = u_n[nx * Y * Z + (ny + 1) * Z + nz];
            u_n_z_left = u_n[nx * Y * Z + ny * Z + (nz - 1)];
            u_n_z_right = u_n[nx * Y * Z + ny * Z + (nz + 1)];
        }
        u_n_center = u_n[nx * Y * Z + ny * Z + nz];
        u_n_1[nx * Y * Z + ny * Z + nz] =
            u_n_center + dt * dt / 2 *
                             nabla(u_n_center, u_n_x_left, u_n_x_right, u_n_y_left, u_n_y_right,
                                   u_n_z_left, u_n_z_right, dx, dy, dz);
    }
}

__host__ void initialize(double *u_n, double *u_n_1, double *u_n_x_left_send_h,
                         double *u_n_y_left_send_h, double *u_n_z_left_send_h,
                         double *u_n_x_left_recv_h, double *u_n_y_left_recv_h,
                         double *u_n_z_left_recv_h, double *u_n_x_right_send_h,
                         double *u_n_y_right_send_h, double *u_n_z_right_send_h,
                         double *u_n_x_right_recv_h, double *u_n_y_right_recv_h,
                         double *u_n_z_right_recv_h, double *u_n_x_left_d, double *u_n_y_left_d,
                         double *u_n_z_left_d, double *u_n_x_right_d, double *u_n_y_right_d,
                         double *u_n_z_right_d) {
    // compute u_0
    int num_blocks = get_num_blocks(X * Y * Z, NUM_THREADS);
    zero_step_kernel<<<num_blocks, NUM_THREADS>>>(u_n, x_low, y_low, z_low, dx, dy, dz, Lx, Ly, Lz,
                                                  X, Y, Z);

    // send border u_0 values
    sendborder(u_n, u_n_x_left_send_h, u_n_y_left_send_h, u_n_z_left_send_h, u_n_x_left_recv_h,
               u_n_y_left_recv_h, u_n_z_left_recv_h, u_n_x_right_send_h, u_n_y_right_send_h,
               u_n_z_right_send_h, u_n_x_right_recv_h, u_n_y_right_recv_h, u_n_z_right_recv_h,
               u_n_x_left_d, u_n_y_left_d, u_n_z_left_d, u_n_x_right_d, u_n_y_right_d,
               u_n_z_right_d);

    // compute u_1
    first_step_kernel<<<num_blocks, NUM_THREADS>>>(
        u_n, u_n_1, x_low, y_low, z_low, dx, dy, dz, dt, Lx, Ly, Lz, X, Y, Z, bnumx, bnumy, bnumz,
        bmax, split_type, u_n_x_left_d, u_n_y_left_d, u_n_z_left_d, u_n_x_right_d, u_n_y_right_d,
        u_n_z_right_d);
    
    // send border u_1 values
    sendborder(u_n_1, u_n_x_left_send_h, u_n_y_left_send_h, u_n_z_left_send_h, u_n_x_left_recv_h,
               u_n_y_left_recv_h, u_n_z_left_recv_h, u_n_x_right_send_h, u_n_y_right_send_h,
               u_n_z_right_send_h, u_n_x_right_recv_h, u_n_y_right_recv_h, u_n_z_right_recv_h,
               u_n_x_left_d, u_n_y_left_d, u_n_z_left_d, u_n_x_right_d, u_n_y_right_d,
               u_n_z_right_d);
}

__global__ void main_step_kernel(double *u_n, double *u_n_1, double *u_n_2, double x_low,
                                 double y_low, double z_low, double dx, double dy, double dz,
                                 double Lx, double Ly, double Lz, int X, int Y, int Z, int bnumx,
                                 int bnumy, int bnumz, int bmax, int split_type,
                                 double *u_n_x_left_d, double *u_n_y_left_d, double *u_n_z_left_d,
                                 double *u_n_x_right_d, double *u_n_y_right_d,
                                 double *u_n_z_right_d, int nsteps, double dt) {
    int ind = blockIdx.x * blockDim.x + threadIdx.x;
    if (ind >= X * Y * Z)
        return;
    int nx = ind / (Y * Z);
    int ny = ind % (Y * Z) / Z;
    int nz = ind % Z;
    double x = x_low + nx * dx;
    double y = y_low + ny * dy;
    double z = z_low + nz * dz;
    double u_n_x_left, u_n_y_left, u_n_z_left, u_n_x_right, u_n_y_right, u_n_z_right;
    double u_n_center;

    if (on_global_border(nx, ny, nz, bnumx, bnumy, bnumz, bmax, split_type, X, Y, Z)) {
        u_n_2[nx * Y * Z + ny * Z + nz] = border(x, y, z, nsteps * dt, Lx, Ly, Lz);
    } else {
        if (split_type == 0) {
            u_n_x_left =
                nx == 0 ? u_n_x_left_d[ny * Z + nz] : u_n_1[(nx - 1) * Y * Z + ny * Z + nz];
            u_n_x_right =
                nx == X - 1 ? u_n_x_right_d[ny * Z + nz] : u_n_1[(nx + 1) * Y * Z + ny * Z + nz];
            u_n_y_left =
                ny == 0 ? u_n_y_left_d[nx * Z + nz] : u_n_1[nx * Y * Z + (ny - 1) * Z + nz];
            u_n_y_right =
                ny == Y - 1 ? u_n_y_right_d[nx * Z + nz] : u_n_1[nx * Y * Z + (ny + 1) * Z + nz];
            u_n_z_left =
                nz == 0 ? u_n_z_left_d[nx * Y + ny] : u_n_1[nx * Y * Z + ny * Z + (nz - 1)];
            u_n_z_right =
                nz == Z - 1 ? u_n_z_right_d[nx * Y + ny] : u_n_1[nx * Y * Z + ny * Z + (nz + 1)];
        } else {
            u_n_x_left =
                nx == 0 ? u_n_x_left_d[ny * Z + nz] : u_n_1[(nx - 1) * Y * Z + ny * Z + nz];
            u_n_x_right =
                nx == X - 1 ? u_n_x_right_d[ny * Z + nz] : u_n_1[(nx + 1) * Y * Z + ny * Z + nz];
            u_n_y_left = u_n_1[nx * Y * Z + (ny - 1) * Z + nz];
            u_n_y_right = u_n_1[nx * Y * Z + (ny + 1) * Z + nz];
            u_n_z_left = u_n_1[nx * Y * Z + ny * Z + (nz - 1)];
            u_n_z_right = u_n_1[nx * Y * Z + ny * Z + (nz + 1)];
        }
        u_n_center = u_n_1[nx * Y * Z + ny * Z + nz];
        u_n_2[nx * Y * Z + ny * Z + nz] =
            2 * u_n_center - u_n[nx * Y * Z + ny * Z + nz] +
            dt * dt *
                nabla(u_n_center, u_n_x_left, u_n_x_right, u_n_y_left, u_n_y_right, u_n_z_left,
                      u_n_z_right, dx, dy, dz);
    }
}

__host__ void main_step(double * &u_n, double * &u_n_1, double * &u_n_2, double *u_n_x_left_send_h,
               double *u_n_y_left_send_h, double *u_n_z_left_send_h, double *u_n_x_left_recv_h,
               double *u_n_y_left_recv_h, double *u_n_z_left_recv_h, double *u_n_x_right_send_h,
               double *u_n_y_right_send_h, double *u_n_z_right_send_h, double *u_n_x_right_recv_h,
               double *u_n_y_right_recv_h, double *u_n_z_right_recv_h, double *u_n_x_left_d,
               double *u_n_y_left_d, double *u_n_z_left_d, double *u_n_x_right_d,
               double *u_n_y_right_d, double *u_n_z_right_d, int nsteps) {
    int num_blocks = get_num_blocks(X * Y * Z, NUM_THREADS);

    // compute u_n_2
    main_step_kernel<<<num_blocks, NUM_THREADS>>>(
        u_n, u_n_1, u_n_2, x_low, y_low, z_low, dx, dy, dz, Lx, Ly, Lz, X, Y, Z, bnumx, bnumy,
        bnumz, bmax, split_type, u_n_x_left_d, u_n_y_left_d, u_n_z_left_d, u_n_x_right_d,
        u_n_y_right_d, u_n_z_right_d, nsteps, dt);

    // send border u_n_2 values
    sendborder(u_n_2, u_n_x_left_send_h, u_n_y_left_send_h, u_n_z_left_send_h, u_n_x_left_recv_h,
               u_n_y_left_recv_h, u_n_z_left_recv_h, u_n_x_right_send_h, u_n_y_right_send_h,
               u_n_z_right_send_h, u_n_x_right_recv_h, u_n_y_right_recv_h, u_n_z_right_recv_h,
               u_n_x_left_d, u_n_y_left_d, u_n_z_left_d, u_n_x_right_d, u_n_y_right_d,
               u_n_z_right_d);

    // swap layers
    double *tmp = u_n;
    u_n = u_n_1;
    u_n_1 = u_n_2;
    u_n_2 = tmp;
}

__host__ void log(double *u_n, int N, int num_processes) {
    double *sendbuf = new double[X * Y * Z];
    cudaMemcpy(sendbuf, u_n, X * Y * Z * sizeof(double), cudaMemcpyDeviceToHost);
    int range[] = {nx_low, nx_high, ny_low, ny_high, nz_low, nz_high};

    if (get_rank(bnumx, bnumy, bnumz) == 0) {
        fstream fs;
        fs.open("log.txt", fstream::out);

        double *u_computed = new double[N * N * N];
        double *recvbuf = new double[N * N * N];
        int *counts = new int[num_processes];
        int *displs = new int[num_processes];
        int *ranges = new int[num_processes * 6];

        MPI_Gather(range, 6, MPI_INT, ranges, 6, MPI_INT, 0, MPI_COMM_WORLD);

        int X, Y, Z, nx_low, ny_low, nz_low;
        for (int rank = 0, shift = 0; rank < num_processes; rank++) {
            X = ranges[rank * 6 + 1] - ranges[rank * 6 + 0];
            Y = ranges[rank * 6 + 3] - ranges[rank * 6 + 2];
            Z = ranges[rank * 6 + 5] - ranges[rank * 6 + 4];
            counts[rank] = X * Y * Z;
            displs[rank] = shift;
            shift += counts[rank];
        }

        MPI_Gatherv(sendbuf, ::X * ::Y * ::Z, MPI_DOUBLE, recvbuf, counts, displs, MPI_DOUBLE, 0,
                    MPI_COMM_WORLD);

        for (int rank = 0; rank < num_processes; rank++) {
            X = ranges[rank * 6 + 1] - ranges[rank * 6 + 0];
            Y = ranges[rank * 6 + 3] - ranges[rank * 6 + 2];
            Z = ranges[rank * 6 + 5] - ranges[rank * 6 + 4];
            nx_low = ranges[rank * 6 + 0];
            ny_low = ranges[rank * 6 + 2];
            nz_low = ranges[rank * 6 + 4];
            for (int nx = 0; nx < X; nx++) {
                for (int ny = 0; ny < Y; ny++) {
                    for (int nz = 0; nz < Z; nz++) {
                        u_computed[(nx_low + nx) * N * N + (ny_low + ny) * N + (nz_low + nz)] =
                            recvbuf[displs[rank] + nx * Y * Z + ny * Z + nz];
                    }
                }
            }
        }

        for (int nx = 0; nx < N; nx++) {
            for (int ny = 0; ny < N; ny++) {
                for (int nz = 0; nz < N; nz++) {
                    fs << u_computed[nx * N * N + ny * N + nz] << endl;
                }
            }
        }

        delete[] u_computed;
        delete[] recvbuf;
        delete[] counts;
        delete[] displs;
        delete[] ranges;
        fs.close();
    } else {
        MPI_Gather(range, 6, MPI_INT, NULL, 6, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Gatherv(sendbuf, X * Y * Z, MPI_DOUBLE, NULL, NULL, NULL, MPI_DOUBLE, 0,
                    MPI_COMM_WORLD);
    }

    delete[] sendbuf;
}

__host__ double compute_delta(double *u_n, double t) {
    double *u_n_host = new double[X * Y * Z];
    double delta = 0;
    double x, y, z;
    cudaMemcpy(u_n_host, u_n, X * Y * Z * sizeof(double), cudaMemcpyDeviceToHost);

    for (int nx = 0; nx < X; nx++) {
        for (int ny = 0; ny < Y; ny++) {
            for (int nz = 0; nz < Z; nz++) {
                x = x_low + nx * dx;
                y = y_low + ny * dy;
                z = z_low + nz * dz;
                double u_n_true = u(x, y, z, t, Lx, Ly, Lz);
                delta = abs(u_n_true - u_n_host[nx * Y * Z + ny * Z + nz]) > delta
                            ? abs(u_n_true - u_n_host[nx * Y * Z + ny * Z + nz])
                            : delta;
            }
        }
    }
    delete[] u_n_host;
    return delta;
}

int main(int argc, char *argv[]) {
    if (argc != 5 and argc != 6) {
        cerr << "Usage: task3 Lx Ly Lz N [--logging]" << endl;
        return 1;
    }

    int rank, num_processes;
    int N, NSTEPS = argc == 6 ? K : 20;
    double delta;

    MPI_Init(&argc, &argv);
    cudaSetDeviceFlags(cudaDeviceScheduleBlockingSync);
    sscanf(argv[1], "%lf", &Lx);
    sscanf(argv[2], "%lf", &Ly);
    sscanf(argv[3], "%lf", &Lz);
    sscanf(argv[4], "%d", &N);

    MPI_Comm_size(MPI_COMM_WORLD, &num_processes);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    setup_range(rank, N + 1, num_processes);

    double *u_n_2, *u_n_1, *u_n;
    cudaMalloc((void **)&u_n, X * Y * Z * sizeof(double));
    cudaMalloc((void **)&u_n_1, X * Y * Z * sizeof(double));
    cudaMalloc((void **)&u_n_2, X * Y * Z * sizeof(double));

    double *u_n_x_left_send_h = new double[Y * Z];
    double *u_n_y_left_send_h = new double[X * Z];
    double *u_n_z_left_send_h = new double[X * Y];
    double *u_n_x_left_recv_h = new double[Y * Z];
    double *u_n_y_left_recv_h = new double[X * Z];
    double *u_n_z_left_recv_h = new double[X * Y];
    double *u_n_x_right_send_h = new double[Y * Z];
    double *u_n_y_right_send_h = new double[X * Z];
    double *u_n_z_right_send_h = new double[X * Y];
    double *u_n_x_right_recv_h = new double[Y * Z];
    double *u_n_y_right_recv_h = new double[X * Z];
    double *u_n_z_right_recv_h = new double[X * Y];

    double *u_n_x_left_d, *u_n_y_left_d, *u_n_z_left_d, *u_n_x_right_d, *u_n_y_right_d,
        *u_n_z_right_d;
    cudaMalloc((void **)&u_n_x_left_d, Y * Z * sizeof(double));
    cudaMalloc((void **)&u_n_y_left_d, X * Z * sizeof(double));
    cudaMalloc((void **)&u_n_z_left_d, X * Y * sizeof(double));
    cudaMalloc((void **)&u_n_x_right_d, Y * Z * sizeof(double));
    cudaMalloc((void **)&u_n_y_right_d, X * Z * sizeof(double));
    cudaMalloc((void **)&u_n_z_right_d, X * Y * sizeof(double));

    double start_time = MPI_Wtime();

    initialize(u_n, u_n_1, u_n_x_left_send_h, u_n_y_left_send_h, u_n_z_left_send_h,
               u_n_x_left_recv_h, u_n_y_left_recv_h, u_n_z_left_recv_h, u_n_x_right_send_h,
               u_n_y_right_send_h, u_n_z_right_send_h, u_n_x_right_recv_h, u_n_y_right_recv_h,
               u_n_z_right_recv_h, u_n_x_left_d, u_n_y_left_d, u_n_z_left_d, u_n_x_right_d,
               u_n_y_right_d, u_n_z_right_d);

    for (int nsteps = 2; nsteps < NSTEPS; nsteps++) {
        main_step(u_n, u_n_1, u_n_2, u_n_x_left_send_h, u_n_y_left_send_h, u_n_z_left_send_h,
                  u_n_x_left_recv_h, u_n_y_left_recv_h, u_n_z_left_recv_h, u_n_x_right_send_h,
                  u_n_y_right_send_h, u_n_z_right_send_h, u_n_x_right_recv_h, u_n_y_right_recv_h,
                  u_n_z_right_recv_h, u_n_x_left_d, u_n_y_left_d, u_n_z_left_d, u_n_x_right_d,
                  u_n_y_right_d, u_n_z_right_d, nsteps);
    }

    delta = compute_delta(u_n_1, (NSTEPS - 1) * dt);
    if (rank == 0) {
        MPI_Reduce(MPI_IN_PLACE, &delta, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    } else {
        MPI_Reduce(&delta, NULL, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    }

    if (rank == 0) {
        cout << "delta " << delta << endl;
        cout << "time " << MPI_Wtime() - start_time << endl;
    }

    if (argc == 6) {
        log(u_n_2, N + 1, num_processes);
    }

    cudaFree(u_n);
    cudaFree(u_n_1);
    cudaFree(u_n_2);
    delete[] u_n_x_left_send_h;
    delete[] u_n_y_left_send_h;
    delete[] u_n_z_left_send_h;
    delete[] u_n_x_left_recv_h;
    delete[] u_n_y_left_recv_h;
    delete[] u_n_z_left_recv_h;
    delete[] u_n_x_right_send_h;
    delete[] u_n_y_right_send_h;
    delete[] u_n_z_right_send_h;
    delete[] u_n_x_right_recv_h;
    delete[] u_n_y_right_recv_h;
    delete[] u_n_z_right_recv_h;
    cudaFree(u_n_x_left_d);
    cudaFree(u_n_y_left_d);
    cudaFree(u_n_z_left_d);
    cudaFree(u_n_x_right_d);
    cudaFree(u_n_y_right_d);
    cudaFree(u_n_z_right_d);

    MPI_Finalize();
    return 0;
}