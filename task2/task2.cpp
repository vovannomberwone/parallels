#include <cmath>
#include <iomanip>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <mpi.h>

using namespace std;

double f(double x, double y, double z) {
    double tmp = 1 + x + y + z;
    return 1 / (tmp * tmp * tmp);
}

double F(double x, double y, double z) {
    return x > 0 and y > 0 and z > 0 and x + y + z < 1? f(x, y, z): 0;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        cerr << "Usage: task2 epsilon" << endl;
        return 1;
    }

    int const portion_size_points = 1000, portion_size_elements = 3 * portion_size_points;
    double const integral = log(2) / (double)2 - (double)5 / 16, upper_volume = 1;
    int rank, num_processes, count;
    double cur_sum = 0, num_points = 0, cur_val = 0, time = 0;
    double eps;
    sscanf(argv[1], "%lf", &eps);
    double points[portion_size_elements];
    MPI_Status status;

    MPI_Init(&argc, &argv);
    double start_time = MPI_Wtime();
    MPI_Comm_size(MPI_COMM_WORLD, &num_processes);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    while (true) {
        if (rank == 0) {
            if (abs(cur_val - integral) < eps) {;
                for (int i = 1; i < num_processes; i++) {
                    MPI_Send(points, 0, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
                }
                time = MPI_Wtime() - start_time;
                MPI_Reduce(MPI_IN_PLACE, &time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);                
                break;
            } else {
                for (int i = 1; i < num_processes; i++) {
                    for (int j = 0; j < portion_size_elements; j++) {
                        // upper volume is symmetric, use 1 generator
                        points[j] = (double)rand() / RAND_MAX;
                    }
                    MPI_Send(points, portion_size_elements, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
                }
                MPI_Reduce(MPI_IN_PLACE, &cur_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
                num_points += portion_size_points * (num_processes - 1);
                cur_val = upper_volume * cur_sum / num_points;
            }
        } else {
            MPI_Recv(points, portion_size_elements, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
            MPI_Get_count(&status, MPI_DOUBLE, &count);
            if (count == 0) {
                time = MPI_Wtime() - start_time;
                MPI_Reduce(&time, NULL, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);                
                break;
            } else {
                cur_sum = 0;
                for (int i = 0; i < portion_size_points; i++) {
                    cur_sum += F(points[3 * i], points[3 * i + 1], points[3 * i + 2]);
                }
                MPI_Reduce(&cur_sum, NULL, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
            }
        }
    }

    if (rank == 0) {
        cout << cur_val << endl;
        cout << abs(cur_val - integral) << endl;
        cout << num_points << endl;
        cout << time << endl;
    }

    MPI_Finalize();
    return 0;
}